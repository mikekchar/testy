// FIXME: Could potentially exhaust stack
// ALSO: I'm not happy with the array hack
const show = (x) => {
  if (typeof(x) !== "object" || !x) return x;
  if (x.show) return x.show();
  if (x.map) return `[${x.map(show)}]`;
  return x;
};

const meta = {
  show: show
};

module.exports = meta;
