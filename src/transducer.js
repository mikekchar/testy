const meta =  {
  xr_pipe: (xform, reducer) =>
    (a, x) =>
      reducer(a, xform(x))
};

module.exports = meta;
