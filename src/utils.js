const meta = {
  last_index: (f, a) => {
    const index = a.slice().reverse().findIndex(f);
    return index >= 0 ? a.length - index - 1 : -1;
  },
  id: (x) => x,
  pipe: (g, f) =>
    (...args) =>
      g(f(...args)),
  zip: (x, y) =>
    x.map((e, i) => [e, y[i]])
};

module.exports = meta;
