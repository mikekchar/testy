const Process = require("process");
const {nothing, just} = require("./maybe.js");

const remove_program = (args) =>
  args.length > 2 ? just(args.slice(2)) : nothing();

const meta = {
  new: remove_program,
  from_system: () =>
    remove_program(Process.argv)
};

module.exports = meta;
