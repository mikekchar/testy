const FS = require("fs");
const Path = require("path");
const Process = require("process");
const {rescue} = require("../src/either.js");

// Wrapper for FS functions, etc.
const meta = {
  // Returns the current working directory
  cwd: Process.cwd,
  // Returns either the contents of the file or an exception
  read: rescue(
    (filename) => (
      {
        code: FS.readFileSync(Path.join(meta.cwd(), filename), "utf8"),
        filename: filename
      }
    )
  )
};

module.exports = meta;
