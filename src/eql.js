const {zip} = require("./utils.js");

// FIXME: Could potentially exhaust stack
// ALSO: I'm not happy with the array hack
const eql = (x, y) => {
  if (x === y) return true;
  if (typeof(x) !== "object" || !x) return x === y;
  if (x.eql) return x.eql(y);
  if (x.length === y.length && x.map) return zip(x, y).every(z => eql(...z));
  return false;
};

const meta = {
  eql: eql
};

module.exports = meta;
