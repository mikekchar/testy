const Meta = require("./meta.js");

const Console = (self, console) => (
  {
    log: (string) =>
      console.log(string)
  }
);

const meta = {
  ...Meta.new(Console),
  from_system: () =>
    meta.new(console)
};

module.exports = meta;
