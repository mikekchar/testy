const Assert = require("./assert.js");
const {rescue} = require("./either.js");
const {last_index} = require("./utils.js");

const scope = {
  require: require
};

const run = (code) =>
  new Function("scope", "with (scope) {" + code + "};")(scope);

const anon_re = /\<anonymous\>:(\d+)/;

const insert_filename = (filename, line) => {
  const no_eval = line.replace(/eval at run \(.*\),*\s/, "");
  const matches = no_eval.match(anon_re);
  if(matches) {
    return no_eval.replace(anon_re, filename + ":" + `${Number(matches[1]) - 2}`);
  } else {
    return no_eval;
  }
};

const stack = (filename, output) => {
  const top = 1;
  const bottom = last_index((x) => x.includes("anonymous"), output);
  const stack = bottom >= 0 ? output.slice(top, bottom + 1) : output.slice(1, 2);
  return stack.map(
    (x) => x.includes("anonymous") ? insert_filename(filename, x) : x
  );
};

const message = (filename, error) => {
  const output = error.stack.split("\n");
  return "In test from " + filename + ":\n\n" + output[0] + "\n" + stack(filename, output).join(",\n") + "\n" + "\n";
};

const meta = {
  run: (code, log) =>
    rescue(run)(code.code).match(
      (error) => log.fail(message(code.filename, error)),
      (results) => log.pass("All tests passed!")
    )
};

module.exports = meta;
