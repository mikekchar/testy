const Meta = require("./meta.js");
const {pipe, id} = require("./utils.js");
const {show} = require("./show.js");
const {eql} = require("./eql.js");

const Maybe = (self, x) => (
  {
    unit: (y) => Meta.instantiate(Maybe, y),
    match: (f_nothing, f_justx) =>
      x === null ? f_nothing() : f_justx(x),
    map: (f) =>
      self().match(self, pipe(self().unit, f)),
    bind: (f) =>
      self().match(self, f),
    withDefault: (def) =>
      self().match(() => def, id),
    show: () =>
      self().match(() =>"nothing()", (y) =>`just(${show(y)})`),
    eql: (m) =>
      m.withDefault ? eql(x, m.withDefault(null)) : false
  }
);

const nothing = Meta.instantiate(Maybe, null);

const meta =  {
  ...Meta.new(Maybe),
  nothing: () => nothing,
  just: (x) => meta.new(x)
};

module.exports = meta;
