// See the README.md file for descriptions of the terms used.

// Returns a new 'self' function.
const Self = (memo) =>
  (self) =>
    self ? (memo = self) : memo;

// Instantiates an object from a constructor
const instantiate = (constructor, ...args) => {
  const self = Self();
  const obj = constructor(self, ...args);
  return self(obj);
};

// Returns a mixin with a new method for the meta object
const Meta = (constructor) => (
  {
    new: (...args) =>
      instantiate(constructor, ...args)
  }
);

// meta object for Meta
const meta = {
  instantiate: instantiate,
  new: Meta
};

module.exports = meta;
