const {show} = require("./show.js");
const {eql} = require("./eql.js");

const fail = (message) => {
  throw new Error(message);
};

const assert = (f, x, message) =>
  f(x) === true ? undefined : fail(message);

const f_returned = (f, x) =>
  `${f.toString()}(${show(x)}) returned ${show(f(x))}`;

const show_not_equal = (x, y) =>
  `${show(x)} !== ${show(y)}`;

const show_equal = (x, y) =>
  `${show(x)} === ${show(y)}`;

const meta = {
  fail: fail,
  assert: f => x =>
    assert(f, x, f_returned(f, x)),
  assert_eql: (x, y) =>
    assert((x) => eql(x, y), x, show_not_equal(x, y)),
  assert_neq: (x, y) =>
    assert((x) => !eql(x, y), x, show_equal(x, y))
};

module.exports = meta;
