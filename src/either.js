const Meta = require("./meta.js");
const {nothing, just} = require("./maybe.js");
const {id, pipe} = require("./utils.js");
const {show} = require("./show.js");

const Either = (self, error, result) => (
  {
    unit: (left, right) =>
      Meta.instantiate(Either, left, right),
    left: (x) =>
      self().unit(just(x), nothing()),
    right: (x) =>
      self().unit(nothing(), just(x)),
    match: (f_error, f_result) =>
      error.match(
        () => f_result(result.withDefault(null)),
        (x) => f_error(x)
      ),
    map: (f) =>
      self().match(
        pipe(self().left, id),
        pipe(self().right, f)
      ),
    bind: (f) =>
      self().match(pipe(self().left, id), f),
    show: () =>
      self().match(
        (left) => `left(${show(left)})`,
        (right) => `right(${show(right)})`
      )
  }
);

const meta =  {
  ...Meta.new(Either),
  left: (x) =>
    meta.new(just(x), nothing()),
  right: (x) =>
    meta.new(nothing(), just(x)),
  rescue: (f) =>
    (...args) => {
      try {
        return meta.right(f(...args));
      } catch (e) {
        return meta.left(e);
      }
    }
};

module.exports = meta;
