const File = require("../src/file.js");
const Log = require("../src/log.js");
const Runner = require("../src/runner.js");
const {xr_pipe} = require("../src/transducer.js");

const run = (log, file_contents) => file_contents.match(
  (error) => log.fail(error.message),
  (code) => Runner.run(code, log)
);

const usage = () =>
  Log.new(["Usage: testy fileglob"], []);

const read_all = (filenames) =>
  filenames.reduce(xr_pipe(File.read, run), Log.new([], []));

const meta =  {
  test: (args) =>
    args.match(usage, read_all)
};

module.exports = meta;
