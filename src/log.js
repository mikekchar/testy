const Meta = require("./meta.js");
const {eql} = require("./eql.js");
const {show} = require("./show.js");

const Log = (self, errors, passed) => (
  {
    unit: (e, p) => Meta.instantiate(Log, e, p),
    fail: (error) =>
      self().unit(errors.concat([error]), passed),
    pass: (pass) =>
      self().unit(errors, passed.concat([pass])),
    errors: () =>
      errors,
    passed: () =>
      passed,
    match:(f_errored, f_passed) =>
      errors.length > 0 ? f_errored(self()) : f_passed(self()),
    eql: (x) =>
      eql(x.errors(), errors) && eql(x.passed(), passed),
    show: () =>
      `Errors: ${show(errors)}, Passed: ${show(passed)}`
  }
);

const meta = Meta.new(Log);

module.exports = meta;
