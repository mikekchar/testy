const Arguments = require("../src/arguments.js");
const {nothing, just} = require("../src/maybe.js");
const {assert_eql} = require("../src/assert.js");

assert_eql(Arguments.new([]), nothing());
assert_eql(Arguments.new(["path"]), nothing());
assert_eql(Arguments.new(["path", "program"]), nothing());
assert_eql(Arguments.new(["path", "program", "file1"]), just(["file1"]));
assert_eql(Arguments.new(["path", "program", "file1", "file2"]), just(["file1", "file2"]));
