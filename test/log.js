const Log = require("../src/log.js");
const {assert_eql} = require("../src/assert.js");

// eql
assert_eql(Log.new([], []), Log.new([], []));

// show
assert_eql(Log.new([], []).show(), "Errors: [], Passed: []");
