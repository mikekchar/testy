const {nothing, just} = require("../src/maybe.js");
const {assert, assert_eql} = require("../src/assert.js");

// Basic equality with eql
assert_eql(nothing(), nothing());
assert_eql(just(5), just(5));

// nothing() returns the same object each time
assert(x => nothing() === x)(nothing());

// just(x) returns a different object each time
assert(x => just(5) !== x)(just(5));

// nothing() matches
nothing().match(
  () => null,
  (x) => fail(`nothing() returned just(${x})`)
);

// just(x) matches
just(5).match(
  () => fail("nothing() was returned"),
  (x) => assert_eql(x, 5, "x !== 5")
);

// unit(x) returns just(x)
just(5).unit(12).match(
  () => fail("Nothing was returned"),
  (x) => assert_eql(x, 12, "x != 12")
);

// map(f)
just(5).map(x => x * 2).match(
  () => fail("Nothing was returned"),
  (x) => assert_eql(x, 10, "x != 10")
);

nothing().map(x => x * 2).match(
  () => null,
  (x) => fail(`returned ${x} instead of nothing()`)
);

// bind(f)
just(5).bind(x => just(x * 2)).match(
  () => fail("Nothing was returned"),
  (x) => assert_eql(x, 10, "x != 10")
);

nothing().bind(x => just(x * 2)).match(
  () => null,
  (x) => fail(`returned ${x} instead of nothing()`)
);

// withDefault(y)
assert_eql(just(5).withDefault(42), 5);
assert_eql(nothing().withDefault(42), 42);

// show
assert_eql(nothing().show(), "nothing()");
assert_eql(just(5).show(), "just(5)");

// recursive show
assert_eql(just(nothing()).show(), "just(nothing())");
