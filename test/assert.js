const {
  fail,
  assert,
  assert_eql,
  assert_neq
} = require("../src/assert.js");
const {nothing, just} = require("../src/maybe.js");

const assertion_message = (f, ...args) => {
  try {
    f(...args);
    return "assertion did not throw exception";
  } catch (e) {
    return e.message;
  }
};

// The correct message is returned on failure
assert_eql(assertion_message(fail, "Error message"), "Error message");
assert_eql(assertion_message(assert(x => x === 5), 4), "x => x === 5(4) returned false");
assert_eql(assertion_message(assert_eql, 1, 2), "1 !== 2");
assert_eql(assertion_message(assert_neq, 2, 2), "2 === 2");

// Does not throw an error
assert(x => x === 5)(5);
assert_eql(2, 2);
assert_neq(1, 2);

// object.show() in error messages when available
const should_be_nothing = (x) =>
  x.match(() => true, (_) => nothing());

assert_eql(
  assertion_message(assert(should_be_nothing), just(5)),
  "(x) =>\n  x.match(() => true, (_) => nothing())(just(5)) returned nothing()"
);
assert_eql(
  assertion_message(assert_eql, nothing(), just(5)),
  "nothing() !== just(5)"
);
assert_eql(
  assertion_message(assert_neq, nothing(), nothing()),
  "nothing() === nothing()"
);
