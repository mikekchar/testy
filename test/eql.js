const {eql} = require("../src/eql.js");
const {just, nothing} = require("../src/maybe.js");
const {assert} = require("../src/assert.js");

// Non-objects
assert(x => eql(12, x))(12);
assert(x => eql("hello", x))("hello");

// Opposite
assert(x => !eql(12, x))(5);
assert(x => !eql("hello", x))("goodbye");

// Null
assert(x => eql(null, x))(null);
assert(x => !eql(null, x))({a: 5});

// With eql
assert(x => eql(just(5), x))(just(5));
assert(x => eql(nothing(), x))(nothing());
assert(x => !eql(just(5), x))(just(4));
assert(x => !eql(nothing(), x))(just(5));

// Mixed
assert(x => !eql(just(5), x))(5);

// Arrays
assert(x => eql([], x))([]);
assert(x => eql([1, 2, 3], x))([1, 2, 3]);
assert(x => !eql([1, 2, 3], x))([1, 2]);
assert(x => !eql([1, 2], x))([1, 2, 3]);
assert(x => !eql([], x))([1, 2, 3]);

// Arrays containing objects
assert(x => eql([just(5)], x))([just(5)]);

// Nested Arrays
assert(x => eql([1, 2, [3]], x))([1, 2, [3]]);
assert(x => eql([[1, []], 2, [3]], x))([[1, []], 2, [3]]);
assert(x => !eql([[1], 2, [3]], x))([[1, []], 2, [3]]);
assert(x => !eql([[1, []], 2, [3]], x))([[1], 2, [3]]);

// Nested Arrays containing objects
assert(x => eql([1, 2, [just(5)]], x))([1, 2, [just(5)]]);
assert(x => !eql([1, 2, [just(5)]], x))([1, 2, [just(4)]]);

// Dictionaries
const dict = {a: 5};

// The same object are eql
assert(x => eql(dict, x))(dict);
// FIXME: Equivalent dictionary objects are not.
assert(x => !eql(dict, x))({a: 5});

// Different dictionaries are not eql
assert(x => !eql(dict, x))({a: 4});

