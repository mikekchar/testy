const {xr_pipe} = require("../src/transducer.js");
const {assert_eql} = require("../src/assert.js");

// Sum up the double of the contents of the array
const t = xr_pipe((x) => x * 2, (a, y) => a + y);
assert_eql([1, 2, 3].reduce(t, 0), 12);
