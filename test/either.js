const {left, right, rescue} = require("../src/either.js");
const {fail, assert_eql} = require("../src/assert.js");

// left(x) indicates an error
left("Error Message").match(
  (error) => assert_eql(error, "Error Message"),
  (result) => fail(`rescue() didn't catch exception and returned ${result}`)
);

// right(x) indicates a result
right(42).match(
  (error) => fail(`Should not have caught exception: ${error}`),
  (result) => assert_eql(result, 42)
);

// map(f)
left("Error Message").map(x => x * 2).match(
  (error) => assert_eql(error, "Error Message"),
  (result) => fail(`rescue() didn't catch exception and returned ${result}`)
);

right(42).map(x => x * 2).match(
  (error) => fail(`Should not have caught exception: ${error}`),
  (result) => assert_eql(result, 84)
);

// bind(f)
left("Error Message").bind(x => right(x * 2)).match(
  (error) => assert_eql(error, "Error Message"),
  (result) => fail(`rescue() didn't catch exception and returned ${result}`)
);

right(42).bind(x => right(x * 2)).match(
  (error) => fail(`Should not have caught exception: ${error}`),
  (result) => assert_eql(result, 84)
);

// rescue() catches an exception and returns an Either
const rescued = (f) => rescue(f);

rescued(() => { throw("Error Message"); }) ().match(
  (error) => assert_eql(error, "Error Message"),
  (result) => fail(`rescue() didn't catch exception and returned ${result}`)
);

rescued(() => 42) ().match(
  (error) => fail(`Should not have caught exception: ${error}`),
  (result) => assert_eql(result, 42)
);

// show()
assert_eql(left(5).show(), "left(5)");
assert_eql(right(5).show(), "right(5)");

// recursive show()
assert_eql(left(right(5)).show(), "left(right(5))");
assert_eql(right(left(5)).show(), "right(left(5))");
