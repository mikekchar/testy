const {show} = require("../src/show.js");
const {assert_eql} = require("../src/assert.js");
const {just, nothing} = require("../src/maybe.js");

// Non-objects should just return themselves
assert_eql(show(5), 5);
assert_eql(show("hello"), "hello");

// null returns null
assert_eql(show(null), null);

// Objects that implement show use it
assert_eql(show(nothing()), "nothing()");

// Arrays
assert_eql(show([]), "[]");
assert_eql(show([1]), "[1]");
assert_eql(show([1, 2]), "[1,2]");

// Nested Arrays
assert_eql(show([1, 2, [3]]), "[1,2,[3]]");
assert_eql(show([[1, []], 2, [3]]), "[[1,[]],2,[3]]");

// Nested Arrays containing objects
assert_eql(show([1, 2, [just(5)]]), "[1,2,[just(5)]]");

// FIXME: Dictionary objects are not handled
