# Coding Standard

Q: Why write a coding standard for a project only I will be modifying?

A: It's the only time I'll get my own way :-)  Actually, because I've found
   that codifying the notation has helped me examine the strategy I'm
   using.  It's easy to get sloppy and gloss over something important.
   I want to write it down so that I can think critically about it.

## Nomenclature in this standard

From strongest to weakest:

- **avoid**       Try not to do this unless absolutely necessary.
                  If you have to do it, then wrap it so that it
                  doesn't leak.
- **prefer**      When there are multiple reasonable options,
                  try to pick this one.  If it makes you jump
                  through hoops, then consider the alternatives.
- **consider**    When writing code related to this, consider
                  this idea.  Feel free to pick any option
                  and when you have enough data to come to
                  a conclusion, then make it consistent and
                  write a **prefer** in its place.

## Function definitions

- Prefer the following form:
  ```
  const func = (args) =>
    process(args);
  ```
- Prefer functions with a single expression over functions with one or
  more statements.  This allows us to elide the braces and return
  statement.  For example, avoid:
  ```
  const func = (args) = {
    return process(args);
  }
  ```

- When writing methods in dictionaries prefer the 'lambda' notation
  rather than ES6's 'method' notation.  For example prefer:
  ```
  {
    method: (args) =>
      process(args)
  }
  ```

  over

  ```
  {
    method(args) {
      process(args)
    }
  }
  ```

  This is arguably controversial, but I prefer to keep all of the
  function definitions looking the same.

- Prefer writing constructors and other functions that return
  dictionaries to do so with a single expression as follows (note the parens):
  ```
  const constructor = (args) = (
    {
      method: () =>
        process(args)
    }
  );
  ```

  Consider putting braces and parens on the same lines.

- Prefer function arguments that are meant to be curried to
  be specified without parentheses, and all on the same line:
  ```
  const add = x => y =>
    x + y;
  ```

  This is a hint to the user that the normal usage of this
  function is to be curried.

- Prefer using parentheses on arguments that are not meant
  to be curried, even if there is only one of them:
  ```
  const print_it = (x) =>
    console.log(x);
  ```

- Consider avoiding currying more than one parameter at a time.  For
  example:
  ```
  const awkward = (x, y) => length =>
    do_something(x, y, length);
  ```

## Mutability

- Avoid mutability.  Use immutable data structures unless there
  is a good performance reason to mutate data.
- Use `const` to define variable where possible.  Avoid `var` and `let`.
- Function parameter variables are *mutable*.  Avoid reassiging them.
- Consider when breaking the above rules,
  ```
  const f = (mutable) =>
    (x) =>
      mutable = g(x);
  ```

  is equivalent to

  ```
  const f = () => {
    let mutable;
    return (x) =>
      mutable = g(x)
  };
  ```

  but allows `mutable` to be initialised.  This may be useful in tests.
  The syntax is unfortunately unintuitive.

## Branching
- Consider subclass polymorphism to avoid branching if possible.
  Note: Somewhat surprising to me, I haven't had a single situation
  where I felt like using subclasses.  I'm not sure why.
- Avoid `if` statements. Use trinary operators where possible.  `if` statements
  are not expressions and the `else` clause is optional.  For example,
  prefer:
  ```
  data.length > 0 ? process(data) : ignore(data);
  ```
  over
  ```
  if (data.length) > 0 {
    process(data);
  }
  ```

## Error handling

- Avoid the use of `null` and `undefined` to handle error
  conditions. Use `Maybe` instead.  Wrap JS library calls
  with `Maybe`.  For example:
  ```
  const index = (f, arr) => {
    const i = arr.findIndex(f);
    return i === undefined ? nothing() : just(i);
  };
  ```
- Use structures (like `Maybe`) that allow you to branch using
  continuations rather than explicit `if` or `switch` statements.
  Prefer:
  ```
  index((c) => c.name == "Bob", customers).match(
    () => console.log("Bob was not found"),
    (i) => console.log("Bob is in position " + i + 1)
  );
  ```

  Over:
  ```
  const i = customers.findIndex((c) => c.name == "Bob");
  if (i === undefinded) {
    console.log("Bob was not found");
  } else {
    console.log("Bob is in position " + i + 1)
  }
  ```
- Avoid exceptions.  Wrap them in `Either.rescue` if you must
  use them.  Exceptions are are side effects and move us
  away from a pure language.
