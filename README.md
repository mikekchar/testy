# Testy

A test framework for JS employing some experimental programming
techniques.  I've been enjoying programming on this project, so
I've slowly gone from having a stance of "This is just a programming
exercise and I will never use this project for anything useful"
to "I might actually use this".

## Current Status

It was super fun, but I've decided to put this on hiatus.  The main
problem is that extensive use of closures in all the popular JS
implementations is quite a bit more expensive that using a
JS object.  There are also considerable memory (and memory leak)
considerations.  To be honest, I wish I could program this way
all the time, but I've decided to concentrate on Rust for
the moment where I have a lot more control over what's going on
under the hood.

Still it was a great experiment and I hope I come back to this
if only to explore the notational potential.

## To Run Testy:

```
bin/testy test/*
```

It will evaluate the test files and report any exceptions that have
been thrown.  If no exceptions are thrown in a file, it will output
"All tests passed".

If the fileglob passed to Testy evaluates to a file that doesn't exist
(or if there is an error loading the file), an error message will be
shown.

See the TODO.org file for a changelog and a description of planned
new work.

## How to run the tests

```
./bin/testy ./test/*
```

OR

```
npm test
```

This will currently report that there is an error in one
test file and in the other test file all tests pass.

## The Code

If you are interested in the source code for this project,
I highly recommend reading the design.md document and
the coding_standard.md document.  It will help you understand
what I'm trying to do.

Note: There is a lot of messy and probably wrong code in
the system right now.  Just trying to bootstrap myself
into a place where I can do TDD.  Now I'm working backwards
to refactor the mess I left :-)

## License

I haven't had time to think about this yet (or if I have,
I forgot about it).  This code will eventually have a free
software license (most likely MIT).  If you wish to use
this code, contact me and I will speed up my decision
making process.

## Contributing

I welcome contributions for all of my projects.  As I've
not actually thought about a license yet (see above), it
makes contributing difficult.  If you wish to contribute,
contact me and I'll make it easy to do so.
