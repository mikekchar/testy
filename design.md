# Design of Testy

Testy started with a blog post that I had intended to write
about how you might design an object oriented system based
more on functional programming ideas.  I was using some
toy code to describe the system I was imagining, but I decided
I needed something more realistic to work through the issues.
Since I like TDD, I wanted to have a test framework compatible
with the philosophy of the system I wanted to build.  For
that reason, I decided to write Testy.

## Definitions

Some of the terms I use in this project are unusual or have
specific meanings that someone else might not understand.
Here are the ones I can think of so far.

- **dictionary**  What JS calls an *object*.
- **object**      A dictionary of functions bound to some data.
- **constructor** A function that binds data to an object using a
                  closure.
- **meta**        Or *meta object*. A dictionary of static functions
                  which includes a `new` function. `new` instantiates
                  an object.  Similar to a *class* in other systems.
- **self**        A function that can be used to access or set the
                  dictionary from within an object.  `self` is always
                  set *after* construction of the object and has no
                  defined value at construction time.
                  **Warning:** *by necessity, `self` is mutable.*
- **mixin**       A dictionary of functions that can be mixed into
                  either an object or meta object at construction
                  time. When mixed into an object, it provides
                  subclass polymorphism.
- **module**      A NodeJS module that contains a *constructor* and a
                  *meta object*. The *meta object* is exported from
                  the *module*.  Within the *module*, the
                  *constructor* is named after the *module* and the
                  *meta object* is called `meta`.  Outside of the
                  module, the *meta object* is named after the
                  *module*.

## Motivation

For most OO systems an object is a data type that has methods bound to
it.  I won't write my blog post here, but imagine that we were to look
at it the other way around: An object is a set of functions that has
data bound to it.  So instead of a normal JS object like:

```
class Rectangle {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }

  area() {
    return this.height * this.width;
  }
}
```

What if we returned a dictionary of functions that close over the data
that we want to use:

```
const Rectangle = (height, width) => (
  {
    area: () =>
      height * width
  }
);
```

There are a couple of nice things about this.  The constructor
`Rectangle()` creates objects and is just a normal function.  The
object it creates is immutable by default: there is no way to access
`height` and `width` outside of the object.  You can make it mutable,
by providing methods that mutate the parameters to `Rectangle()`, but
you have to go out of your way to do that.  Even if you have an
accessor, as long as the data held by the parameter is immutable,
the object remains immutable.  For example:

```
const Rectangle = (height, width) => (
  {
    area: () =>
      height * width,
    height: () =>
      height,
    width: () =>
      width
  }
);
```

If height and width are numbers, for example, because numerical
values are immutable, you can't change them and the object remains
immutable.

You can even easily mix in other classes.  Let's say we have
another constructor called `Shape(height, width)` (which I won't
write, but possibly it does things like calculate bounding boxes,
etc).  We can do this very easily:

```
const Rectangle = (height, width) => (
  {
    ...Shape(height, width),
    area: () =>
      height * width,
    height: () =>
      height,
    width: () =>
      width
  }
);
```

And now our Rectangle has mixed in a Shape object.

It gets a bit complicated if you want to have something like
a `this` pointer.  I won't get into the details, but please
refer to the file `src/meta.js` to see how I accomplish this.
(Spoiler: it requires a single mutable variable).  But
here's what our Rectangle might look like in the current system
used my Testy:

```
const Meta = require("./meta.js");
const Shape = require("./shape.js");

const Rectangle = (self, height, width) => (
  {
    ...Shape(self, height, width),
    area: () =>
      height * width,
    is_bigger_than: (rect) =>
      self().area > rect.area(),
    height: () =>
      height,
    width: () =>
      width
  }
);

const meta =  {
  ...Meta.new(Either)
}

module.exports = meta;
```

I've added a method called `is_bigger_than()` that takes another
rectangle and tells you if this rectangle is bigger than the one
passed in.  `self` is a function that returns the current object. The
magic for setting up `self` is in the `Meta` module.  To use rectangle
in a test you would do something like:

```
{assert_eql} = require("./assert.js");
Rectangle = require("./rectangle.js");

const rect1 = Rectangle.new(5, 3);
const rect2 = Rectangle.new(2, 1);

assert_eql(rect2.is_bigger_than(rect1), true);
```

With this setup, we can conveniently make immutable objects.  We can
easily mix in mixins and we can even have subclass polymorphism
(though there is no easy way to provide a `super()` function). What
would writing code like this look like?

That's the question that Testy was written to answer.
